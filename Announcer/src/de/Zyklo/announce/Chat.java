package de.Zyklo.announce;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import net.md_5.bungee.api.ChatColor;

public class Chat implements Listener{

	@EventHandler(priority = EventPriority.LOWEST)
	public void onChat(AsyncPlayerChatEvent event){
		if(event.isCancelled()){
			return;
		}
		final String name = event.getPlayer().getName();
		String msg = event.getMessage();
		if(name.equals("LordUnbekannt") || name.equals("Zyklodanic") || name.equals("daJavaCup")){
			event.setFormat(ChatColor.GRAY + "UAD " + ChatColor.WHITE + "" + ChatColor.BOLD + name + ChatColor.AQUA + " " + msg);
		} else {
			event.setFormat(ChatColor.DARK_GRAY + name + ChatColor.GREEN + " " + msg);
		}
		
	}
	
}
