package de.Zyklo.announce;

import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;

public class PlayerOutStream extends PrintStream {
	private Player p;
	public PlayerOutStream(Player p) throws NullPointerException, FileNotFoundException{
		this("latest.log",p);
	}
	public PlayerOutStream(String fileName, Player p) throws NullPointerException, FileNotFoundException {
		super(fileName);
		this.p = p;
	}
	@Override
	public void println(String x) {
		PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;
        IChatBaseComponent icbc = ChatSerializer.a("{\"text\": \"" + "�a�lJoin �e" + "�e"+x + "\"}");
        PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte) 2);
        connection.sendPacket(bar);
	}
	@Override
	public void println(boolean x) {
		this.println(Boolean.toString(x));
	}
	@Override
	public void print(String s) {
		this.println(s);
	}
	@Override
	public void println() {
		this.print("");
	}
	@Override
	public void print(boolean b) {
		this.println("" + b);
	}
	@Override
	public void print(char c) {
		this.print("" + c);
	}
	@Override
	public void print(char[] s) {
		this.print("" + s.toString());
	}
	@Override
	public void print(double d) {
		this.print("" + d);
	}
	@Override
	public void print(float f) {
		this.print("" + f);
	}
	@Override
	public void print(int i) {
		this.print("" + i);
	}
	@Override
	public void print(long l) {
		this.print("" + l);
	}
	@Override
	public void print(Object obj) {
		this.println(obj.toString());
	}
	@Override
	public void println(char x) {
		this.println("" + x);
	}
	@Override
	public void println(char[] x) {
		this.println("" + x.toString());
	}
	@Override
	public void println(double x) {
		this.println("" + x);
	}
	@Override
	public void println(float x) {
		this.println("" + x);
	}
	@Override
	public void println(int x) {
		this.print("" + x);
	}
	@Override
	public void println(long x) {
		this.print("" + x);
	}
	@Override
	public void println(Object x) {
		this.print("" + x.toString());
	}
	
}
