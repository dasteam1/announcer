package de.Zyklo.announce;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {

	public static String username;
	public static String password;
	public static String database;
	public static String host;
	public static String port;
	public static Connection conn;

	public static void connect(){
		
			try {
				conn = DriverManager.getConnection("jdbc:mysql://"+ host + ":" + port + "/" + database + "?autoReconnect=true", username, password);
				System.out.println("[MySQL] Verbunden!");
			} catch (SQLException e) {
				System.err.println("Konnte nicht verbunden werden. ("+e.getMessage()+")");
			}
		
	}
	

	
public static void close(){
		if(isConnected()){
			try {
				conn.close();
				conn = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

public static boolean isConnected(){
	
	return conn != null;
}



public static void table(){
	if(!MySQL.isConnected()){
		return;
	}
	MySQL.update("CREATE TABLE IF NOT EXISTS `"+database+"`.`MESSAGES` ( `MESSAGE` VARCHAR(666) NOT NULL ) ENGINE = InnoDB;");
}

public static void update(String qry){
	if(!MySQL.isConnected()){
		MySQL.connect();
	}
		try {
			conn.createStatement().executeUpdate(qry);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
}

public static ResultSet getResult(String qry){
	if(!MySQL.isConnected()){
		MySQL.connect();
	}
		 try {
			return conn.createStatement().executeQuery(qry);
		} catch (SQLException e) {
			System.err.println("Fehler beim ResultSet: "+e.getMessage());
		}
	
	return null;
}
}
