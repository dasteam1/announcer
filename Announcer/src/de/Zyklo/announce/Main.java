package de.Zyklo.announce;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.UnknownDependencyException;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Main extends JavaPlugin implements Listener {

	public static Main INSTANCE;
	public List<String> msgList;
	private PluginManager pm;
	private int counter;
	private int c;
	boolean isprefix = false;
	String prefix = "";
	boolean isMySQL = false;
	
	private int getC(){
		return this.c;
	}
	public void onEnable() {
		INSTANCE = this;
		super.onEnable();
		pm = Bukkit.getPluginManager();
		counter = 0;
		registerEvent();
		registerConfig();
		
		
			if(isMySQL == true){
			MySQL.connect();
			MySQL.table();
			ResultSet rs = MySQL.getResult("SELECT * FROM `MESSAGES`");
			
			try {
				while(rs.next()){
					msgList.add(rs.getString("MESSAGE"));
				}
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			MySQL.close();
			}

		
		if(isMySQL == false){
			msgList = this.getConfig().getStringList("Messages");
		}
		c = this.getConfig().getInt("Period");
		if (msgList.isEmpty()) {
			System.out.println("[Announcer] No messages were found.");
			return;
		}
		
		/*new BukkitRunnable() {

			@Override
			public void run() {

			
				if(isprefix){
					Bukkit.broadcastMessage(prefix + " " +ChatColor.translateAlternateColorCodes(
							'&', msgList.get(counter)));
				}else{
					Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes(
						'&', msgList.get(counter)));
				}
				
				counter++;
				if (counter > msgList.size() - 1) {
					counter = 0;
				}
			}

		}.runTaskTimer(this, 0, c * 20l);*/

	}

	public void onDisable() {
		super.onDisable();
		this.getConfig().set("Messages", msgList);
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		/*
		try {
			Plugin pl = Bukkit.getPluginManager().loadPlugin(this.getFile());
			Bukkit.getPluginManager().enablePlugin(pl);
			} catch (UnknownDependencyException | InvalidPluginException | InvalidDescriptionException e) {}
			*/
	}

	private void registerEvent() {
		pm.registerEvents(this, this);
		pm.registerEvents(new Hax(), this);
		//pm.registerEvents(new Chat(), this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(command.getName().equalsIgnoreCase("addMsg")){
			if(sender.hasPermission("announcer.addMessages")){
				String msg = String.join(" ", args);
				msgList.add(msg);
			}
		}
		return true;
	}
	private void registerConfig() {
		this.getConfig().options().header("'Period' is the time the messages are displayed one after one.\n"
				+ "'Messages' is the List of messages to be displayed.\n"
				+ "'prefix' is the prefix displayed in front of the messages."
				+ "");
		this.getConfig().addDefault("Period", 2000000000);
		ArrayList<String> list = new ArrayList<String>();
		list.add("&");
		this.getConfig().addDefault("Messages", list);
		this.getConfig().addDefault("enablePrefix", false);
		this.getConfig().addDefault("prefix", "[?]");
		
		
		this.getConfig().addDefault("mysqlenabled", false);
		this.getConfig().addDefault("host", "localhost");
		this.getConfig().addDefault("database", "pikachu");
		this.getConfig().addDefault("user", "user");
		this.getConfig().addDefault("password", "xxx");
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		
		isprefix = this.getConfig().getBoolean("enablePrefix");
		prefix = this.getConfig().getString("prefix");
		
		MySQL.database = getConfig().getString("database");
		MySQL.host = getConfig().getString("host");
		MySQL.username = getConfig().getString("user");
		MySQL.password = getConfig().getString("password");
	}

}
